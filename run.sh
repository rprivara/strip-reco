#!/bin/bash

# Print usage/help statements
# No params
print_help() {
    echo -e "\e[30mUsage   : bash run.sh [run_n_start] [run_n_end] [run_n_align]\e[0m"
    echo -e "\e[30mExample : bash run.sh 2081 2120 2090\e[0m"
}

# Look up run geometry of the given run number
# Param 1: run number
retrieve_geometry() {
    map_line=(`grep $1 ${data_dir}/map`)
    echo ${map_line[1]}
}

# Look up DUT threshold of the given run number
# Param 1: run number
retrieve_DAC() {
    map_line=(`grep $1 ${data_dir}/map`)
    echo ${map_line[2]}
}

# Look up run status of the given run number
# Param 1: run number
retrieve_status() {
    map_line=(`grep $1 ${data_dir}/map`)
    status=${map_line[3]}
    if [ "$status" == "ok" ]; then
        echo "\e[32m${map_line[3]}\e[0m"
    elif [ "$status" == "warn" ]; then
        echo "\e[33m${map_line[3]}\e[0m"
    elif [ "$status" == "bad" ]; then
        echo "\e[31m${map_line[3]}\e[0m"
    fi
}

get_workers() {
    echo $(($(ps --no-headers -o pid --ppid=${MAIN_PID} | wc -w)-1))
}

mem_proc() {
    ps --no-headers --ppid=${MAIN_PID} -o rss | awk '{rss += $1/1024} END {print rss}'
}

mem_used() {
    free -m | awk 'FNR == 2 {print $3}'
}

mem_avail() {
    free -m | awk 'FNR == 2 {print $7}'
}

mem_avail_per() {
    free -m | awk 'FNR == 2 {print $7/$2*100}'
}

time_() {
    echo "[$(date +%H:%M:%S)]"
}

ready() {
    # If run is prealignment, always valid
    if [[ $2 == "p" ]]; then
        echo 1
    else
        # See if earlier (pre-)alignment stages are done
        CHK_STAGES=( p $(seq 1 $((${STAGE}-1))) )

        READY=1
        for CHK in ${CHK_STAGES[@]}; do
            if [[  ${RUNS["${1}_${CHK}"]} -eq 0 ]]; then
                # echo "$N not done"
                READY=0
                break
            fi
        done

        echo $READY
    fi
}

waiting() {
    PRINT_NEWLINE=0
    WAIT_FOR=1

    ACT_THREADS=$(get_workers)
    MEM_LIM=$(echo "$(mem_avail_per) < ${AVAIL_MEM_LIM}" | bc -l)

    while [[ ${ACT_THREADS} -ge $WORKERS ]] || (( ${MEM_LIM} )); do

        [[ ${ACT_THREADS} -ge $WORKERS ]] && W_COL="\e[31m" || W_COL="\e[33m"
        (( ${MEM_LIM} )) && M_COL="\e[31m" || M_COL="\e[33m"

        # echo -en "\r$(time_) Waiting [WOR_OCC = ${W_COL}${ACT_THREADS}/$WORKERS\e[0m, MEM_USED = \e[33m$(mem_used) MB\e[0m, MEM_PROC = \e[33m$(mem_proc) MB\e[0m, MEM_AVAIL = ${M_COL}$(mem_avail) MB ($(mem_avail_per) %)\e[0m]    "
        sleep $WAIT_FOR

        ACT_THREADS=$(get_workers)
        MEM_LIM=$(echo "$(mem_avail_per) < ${AVAIL_MEM_LIM}" | bc -l)

        PRINT_NEWLINE=1
    done

    # [[ $PRINT_NEWLINE -eq 1 ]] && echo
}

check_finished() {
    for RUN in ${!PID_MAP[@]}; do
        # echo -n  "Checking if process ${PID_MAP[$RUN]} (run $RUN) is still running: "
        if [[ $(ps --no-headers -p ${PID_MAP[$RUN]} | wc -l) -eq 0 ]]; then
            RUNS[$RUN]=1
            unset PID_MAP[$RUN]
        fi
        # echo
    done
}

runs_remaining() {
    REM=0
    for R in ${!RUNS[@]}; do
        [[ ${RUNS[$R]} -ne 1 ]] && REM=$((REM+1))
    done
    echo $REM
}

# Runs masking stage of the reconstruction
# No params
run_masking() {
    echo -en "\e[34;1m[MASKING]   \e[0m"

    # Check if masking already done
    if [[ -d ${out_dir}/MaskCreator && -n "$(ls -A ${out_dir}/MaskCreator)" && $override -eq 0 ]]; then
        echo -e "\e[33mMasking files already exist, skipping.\e[0m"
        return 0
    fi

    # Check that run isn't bad
    if [[ $(retrieve_status ${run_n_align}) =~ "bad" ]]; then
        echo -e "Run ${run_n_align} is \e[31mbad\e[0m, aborting."
        return 1
    fi

    echo -e "Running on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
    ${corry_exec} \
        -c ${conf_dir}/mask.conf \
        -o detectors_file=${conf_dir}/geometries/${geom}.conf \
        -o detectors_file_updated=${geom_dir}/mask.conf \
        -o output_directory=${out_dir} \
        -o histogram_file=mask.root \
        -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
        -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
        -o log_file=${log_dir}/mask.log > /dev/null
}

# Runs prealignment stage of the reconstruction
# No params
run_prealign() {
    echo -en "\e[34;1m[PRE-ALIGN] \e[0m"

    # Check if prealignment already done
    if [[ -f ${geom_dir}/prealign.conf && $override -eq 0 ]]; then
        echo -e "\e[33mPrealignment output already exists, skipping.\e[0m"
        return 0
    fi

    # Check that run isn't bad
    if [[ $(retrieve_status ${run_n_align}) =~ "bad" ]]; then
        echo -e "Run ${run_n_align} is $(retrieve_status ${run_n_align}), aborting."
        return 1
    fi

    echo -e "Running on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
    ${corry_exec} \
        -c ${conf_dir}/prealign_tele.conf \
        -o detectors_file=${geom_dir}/mask.conf \
        -o detectors_file_updated=${geom_dir}/prealign.conf \
        -o output_directory=${out_dir} \
        -o histogram_file=prealign_tele.root \
        -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
        -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
        -o log_file=${log_dir}/prealign_tele.log > /dev/null
}

# Runs alignment stage of the reconstruction
# Param 1 (bool): Telescope alignment
# Param 2 (bool): DUT alignment
run_align() {
    echo -ne "\e[34;1m[ALIGN]     \e[0mFlags: \e[0m"
    if [ $1 -eq 1 ]; then echo -en "\e[32mTele \e[0m"; else echo -en "\e[31mTele \e[0m"; fi
    if [ $2 -eq 1 ]; then echo -en "\e[32mDUT \e[0m"; else echo -en "\e[31mDUT \e[0m"; fi
    echo

    # If telescope alignment already done, skip it
    if [[ $1 -eq 1  ]] && [ -f ${geom_dir}/telescope.conf ] && [ $override -eq 0 ]; then
        echo -e "            \e[33mTelescope alignment already done, skipping.\e[0m"
        skip_telescope=1
    fi

    if [[ $1 -eq 1 && skip_telescope -ne 1 ]]; then
        # Check that run is not bad
        if [[ $(retrieve_status ${run_n_align}) =~ "bad" ]]; then
            echo -e "            Run ${run_n_align} is $(retrieve_status ${run_n_align}), aborting telescope alignment."
            return 1
        fi

        echo -e "            Running telescope alignment 1 on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
        ${corry_exec} \
            -c ${conf_dir}/align_tele1.conf \
            -o detectors_file=${geom_dir}/prealign.conf \
            -o detectors_file_updated=${geom_dir}/telescope.conf \
            -o output_directory=${out_dir} \
            -o histogram_file=align_tele1.root \
            -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
            -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
            -o log_file=${log_dir}/align_tele1.log > /dev/null

        echo -e "            Running telescope alignment 2 on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
        ${corry_exec} \
            -c ${conf_dir}/align_tele2.conf \
            -o detectors_file=${geom_dir}/telescope.conf \
            -o detectors_file_updated=${geom_dir}/telescope.conf \
            -o output_directory=${out_dir} \
            -o histogram_file=align_tele2.root \
            -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
            -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
            -o log_file=${log_dir}/align_tele2.log > /dev/null

        echo -e "            Running telescope alignment 3 on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
        ${corry_exec} \
            -c ${conf_dir}/align_tele3.conf \
            -o detectors_file=${geom_dir}/telescope.conf \
            -o detectors_file_updated=${geom_dir}/telescope.conf \
            -o output_directory=${out_dir} \
            -o histogram_file=align_tele3.root \
            -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
            -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
            -o log_file=${log_dir}/align_tele3.log > /dev/null

        echo -e "            Running telescope alignment 4 on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
        ${corry_exec} \
            -c ${conf_dir}/align_tele4.conf \
            -o detectors_file=${geom_dir}/telescope.conf \
            -o detectors_file_updated=${geom_dir}/telescope.conf \
            -o output_directory=${out_dir} \
            -o histogram_file=align_tele4.root \
            -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
            -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
            -o log_file=${log_dir}/align_tele4.log > /dev/null

        echo -e "            Running telescope alignment 5 on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
        ${corry_exec} \
            -c ${conf_dir}/align_tele5.conf \
            -o detectors_file=${geom_dir}/telescope.conf \
            -o detectors_file_updated=${geom_dir}/telescope.conf \
            -o output_directory=${out_dir} \
            -o histogram_file=align_tele5.root \
            -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
            -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
            -o log_file=${log_dir}/align_tele5.log > /dev/null

        echo -e "            Running telescope alignment 6 on run ${run_n_align} (DAC $(retrieve_DAC ${run_n_align}), status $(retrieve_status ${run_n_align}))."
        ${corry_exec} \
            -c ${conf_dir}/align_tele6.conf \
            -o detectors_file=${geom_dir}/telescope.conf \
            -o detectors_file_updated=${geom_dir}/telescope.conf \
            -o output_directory=${out_dir} \
            -o histogram_file=align_tele6.root \
            -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${run_n_align}.raw \
            -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${run_n_align}.raw \
            -o log_file=${log_dir}/align_tele6.log > /dev/null
    fi

    # DUT alignment
    # Array for runs and process IDs
    declare -A RUNS
    declare -A PID_MAP

    # Generate array of runs
    for R in $(seq $run_n_start $run_n_end); do
        # Skip bad runs
        status=$(retrieve_status $R)
        if [[ -z "$status" ]] || [[ "$status" =~ "bad" ]]; then
            continue
        fi

        RUNS[${R}_p]=0
        for N in $(seq 1 4); do
            RUNS[${R}_${N}]=0
        done
    done

    # Process alignment runs
    if [ $2 -eq 1 ]; then
        while [[ $(runs_remaining) -ne 0 ]]; do
            # Check if any running process has finished
            check_finished

            for R in ${!RUNS[@]}; do
                # Skip if already finished or running
                if [[ ${RUNS[$R]} -eq 1 ]] || [[ -n "${PID_MAP[$R]}" ]] ; then
                    continue
                fi

                # Wait if workers are occupied or memory usage is high
                waiting

                # Check if run can be submitted
                RUN=$(echo "$R" | cut -f 1 -d "_")
                STAGE=$(echo "$R" | cut -f 2 -d "_")
                if [[ $(ready $RUN $STAGE) -ne 1 ]]; then
                    continue
                fi

                # Choose correct geometry file based on alignment stage
                if [[ $STAGE == "p" ]]; then
                    DET_FILE="${geom_dir}/telescope.conf"
                    CONF_FILE="${conf_dir}/prealign_dut.conf"
                    HIST_FILE="run${RUN}_prealign.root"
                    LOG_FILE="${log_dir}/run${RUN}_prealign.log"
                else
                    DET_FILE="${geom_dir}/run${RUN}.conf"
                    CONF_FILE="${conf_dir}/align_dut${STAGE}.conf"
                    HIST_FILE="run${RUN}_align${STAGE}.root"
                    LOG_FILE="${log_dir}/run${RUN}_align${STAGE}.log"
                fi

                # Submit run
                echo -en "$(time_) Submitting run \e[34;1m$RUN [$STAGE]\e[0m"
                ${corry_exec} \
                -c ${CONF_FILE} \
                -o detectors_file=${DET_FILE} \
                -o detectors_file_updated=${geom_dir}/run${RUN}.conf \
                -o output_directory=${out_dir} \
                -o histogram_file=${HIST_FILE} \
                -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${RUN}.raw \
                -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${RUN}.raw \
                -o AnalysisItkStripEfficiency.file_ttc=${data_dir}/run00${RUN}.raw \
                -o log_file=${LOG_FILE} > /dev/null &

                # Enter PID of the started run into a map
                PID=$!
                PID_MAP[$R]=$PID

                echo -e ", remaining \e[34;1m$(($(runs_remaining)-${#PID_MAP[@]}))\e[0m "

                # Check running processes
                check_finished

                sleep 1
            done

            # If all runs have been submitted, end the loop
            [[ $(($(runs_remaining)-${#PID_MAP[@]})) -eq 0 ]] && break

            sleep 1
        done

        echo "$(time_) Waiting for final runs to finish"
        wait
        echo "$(time_) Done"
    fi
}


# Runs alignment stage of the reconstruction
# No params
run_analysis() {
    echo -e "\e[34;1m[ANALYSIS]\e[0m  Running."

    RUNS=$(seq $run_n_start $run_n_end)
    REM_RUNS=(${RUNS[@]})

    for R in ${RUNS[@]}; do
        status=$(retrieve_status $R)
        if [[ -z "$status" ]] || [[ "$status" =~ "bad" ]]; then
            continue
        fi

        # Wait if too many threads are running
        waiting

        echo -ne "$(time_) Submitting run \e[34;1m${R}\e[0m run"

        ${corry_exec} \
            -c ${conf_dir}/analyze.conf \
            -o detectors_file=${geom_dir}/run${R}.conf \
            -o output_directory=${out_dir} \
            -o histogram_file=run${R}_analysis.root \
            -o EventLoaderEUDAQ2.file_name=${data_dir}/run00${R}.raw \
            -o EventLoaderEUDAQ2Multi.file_name=${data_dir}/run00${R}.raw \
            -o log_file=${log_dir}/run${R}_analysis.log \
            -o AnalysisItkStripEfficiency.file_ttc=${data_dir}/run00${R}.raw > /dev/null &

        # Remove submitted run from runlist
        REM_RUNS=( ${REM_RUNS[@]:1} )

        echo -e " (runs remaining = \e[34;1m${#REM_RUNS[@]}\e[0m)"

        sleep 1

        # eff=`cat ${CORRY_LOG_DIR}/thr${THR}_analysis.log | grep 'Total efficiency of detector' | grep -oE :[[:space:]][0-9]*.*%`
        # total=`echo ${eff%(*} | grep -oE [0-9].*`
        # echo -e " efficiency = $total%"
    done

    echo "$(time_) Waiting for final runs to finish"
    wait
    echo "$(time_) Done"
}

extract_efficiencies() {
    echo -e "\e[34;1mExtracting efficiencies from log files:\e[0m"

    # Make a new csv file
    rm -f ${res_dir}/efficiency.csv
    touch ${res_dir}/efficiency.csv

    # Write csv file header
    echo -e "#run, thr, eff, eff_err_up, err_err_down" >> ${res_dir}/efficiency.csv

    # Variable to hold a threshold if it is to be skipped
    skip_DAC=""

    # Iterate over thresholds
    for run in $(seq ${run_n_start} ${run_n_end}); do
        # Skip run if either bad or is already included by an earlier split run
        if [[ "$(retrieve_status $run)" =~ "bad" ]] || [[ "$(retrieve_DAC $run)" == $skip_DAC ]]; then
            continue
        fi

        # Skip if analysis log file doesn't exist
        if [ ! -f ${log_dir}/run${run}_analysis.log ]; then
            continue
        fi

        echo -e "            \e[34;1mRun $run\e[0m (DAC $(retrieve_DAC ${run}), status $(retrieve_status ${run}))"

        # Count the number of runs with this threshold (for split runs)
        thr=$(retrieve_DAC ${run})
        same_thr=( $(grep "[[:space:]]$thr[[:space:]]" ${data_dir}/map | grep $geom | grep -oE "^[0-9]*[[:space:]]") )
        same_thr_ok=("${same_thr[@]}")

        # Remove bad runs from the split run list
        for t in ${same_thr[@]}; do
            if [[ "$(retrieve_status $t)" =~ "bad" ]]; then
                same_thr_ok=( ${same_thr_ok[@]/$t} )
            fi
        done

        # If threshold is split across multiple runs, recalculate efficiency
        if [ ${#same_thr_ok[@]} -gt 1 ]; then
            skip_DAC=$(retrieve_DAC $run)
            echo -en "              - This run has a threshold that is split across ${#same_thr_ok[@]} good runs:\e[34;1m "
            for r in ${same_thr_ok[@]}; do echo -n "$r "; done
            echo -en "\e[0m\n              - Calculating combined efficiency: "

            # Get numbers of matched and accepted tracks for split runs
            matched_tracks=0
            total_tracks=0
            for r in ${same_thr_ok[@]}; do
                mat_tot_ratio=$(grep "matched/total tracks" ${log_dir}/run${r}_analysis.log | grep -oE [0-9]+\/[0-9]+)
                mat_tot_ratio=( $(echo $mat_tot_ratio | tr "/" " ") )
                matched_tracks=$((matched_tracks + mat_tot_ratio[0]))
                total_tracks=$((total_tracks + mat_tot_ratio[1]))
            done
            echo -en " ${matched_tracks}/${total_tracks} --> "

            # Call root macro to calculate efficiency and errors using Pearson-Clopper
            result=( $(root -l -b -q "calc_eff_errors.C(${matched_tracks},${total_tracks})" | grep -oE "^[0-9]+.*") )
            total=${result[0]}
            echo "$total%"
            er_low=${result[1]}
            er_up=${result[2]}
        else
            eff=`cat ${log_dir}/run${run}_analysis.log | grep -E 'Total efficiency of detector dut' | grep -oE :[[:space:]][0-9]*.*%`
            if [ -z "$eff" ]; then
                echo "              - Empty efficiency string."
                continue
            fi
            total=`echo ${eff%(*} | grep -oE [0-9].*`
            er_up=`echo ${eff} | grep -oE "\(.*[[:space:]]" | grep -oE "[0-9].*"`
            er_up=${er_up:0:-1}
            er_low=`echo ${eff##*-} | grep -oE ".*)"`
            er_low=${er_low:0:-1}
        fi

        # Write efficiency and errors into the csv file
        echo "$run,$(retrieve_DAC $run),$total,$er_up,$er_low"  >> ${res_dir}/efficiency.csv

        mv ${res_dir}/efficiency.csv ${res_dir}/efficiency_unsorted.csv

        # Sort the efficiency file by threshold
        sort -t',' -k2,2 -g ${res_dir}/efficiency_unsorted.csv -o ${res_dir}/efficiency.csv

        rm ${res_dir}/efficiency_unsorted.csv
    done
}

extract_cluster_sizes () {
    echo -e "\e[34;1mExtracting mean cluster sizes from analysis output files:\e[0m"

    # Make new csv file
    rm -f ${res_dir}/clusters_unsorted.csv
    touch ${res_dir}/clusters_unsorted.csv

    # Write csv file header
    echo -e "#run, thr, mean_cluster_size, mean_error" >> ${res_dir}/clusters_unsorted.csv

    # Variable to hold a threshold if it is to be skipped
    skip_DAC=""

    # Iterate over thresholds
    for run in $(seq ${run_n_start} ${run_n_end}); do
        # Skip run if either bad or is already included by an earlier split run
        if [[ "$(retrieve_status $run)" =~ "bad" ]] || [[ "$(retrieve_DAC $run)" == $skip_DAC ]]; then
            continue
        fi

        # Skip if output file doesn't exist
        if [ ! -f ${out_dir}/run${run}_analysis.root ]; then
            continue
        fi

        echo -en "            \e[34;1mRun $run\e[0m (DAC $(retrieve_DAC ${run}), status $(retrieve_status ${run})):"

        # Count the number of runs with this threshold (for split runs)
        thr=$(retrieve_DAC ${run})
        same_thr=( $(grep "[[:space:]]$thr[[:space:]]" ${data_dir}/map | grep $geom | grep -oE "^[0-9]*[[:space:]]") )
        same_thr_ok=("${same_thr[@]}")

        # Remove bad runs from the split run list
        for t in ${same_thr[@]}; do
            if [[ "$(retrieve_status $t)" =~ "bad" ]]; then
                same_thr_ok=( ${same_thr_ok[@]/$t} )
            fi
        done

        # If threshold is split across multiple runs, calculate combined cluster size means
        if [ ${#same_thr_ok[@]} -gt 1 ]; then
            skip_DAC=$(retrieve_DAC $run)
            echo -en "\n              - This run has a threshold that is split across ${#same_thr_ok[@]} good runs:\e[34;1m "
            for r in ${same_thr_ok[@]}; do echo -n "$r "; done
            echo -en "\e[0m\n              - Calculating combined cluster size:"

            # Remove bad runs from the split run list
            runs=""
            for t in ${same_thr[@]}; do
                runs+="$t,"
            done
            runs=${runs::-1}

            output=$(root -l -b -q "calc_cluster_sizes.C(\"${CAMPAIGN}\",\"${geom}\",\"$runs\")")
        else
            output=$(root -l -b -q "calc_cluster_sizes.C(\"${CAMPAIGN}\",\"${geom}\",\"$run\")")
        fi

        # echo $output
        mean=$(echo $output | grep -oE "Mean=.*," | grep -oE "[0-9.]*")
        error=$(echo $output | grep -oE "Error=.*\(" | grep -oE "[0-9.]*")
        echo " $mean +- $error"

        # Write to csv file
        if [[ "$mean" != "0" ]] && [[ ! -z "$mean" ]]; then
            echo "$run,$(retrieve_DAC ${run}),$mean,$error" >> ${res_dir}/clusters_unsorted.csv
        fi
    done

    # Sort the cluster size file by threshold
    sort -t',' -k2,2 -g ${res_dir}/clusters_unsorted.csv -o ${res_dir}/clusters.csv

    rm ${res_dir}/clusters_unsorted.csv
}

convert_position() {
    # echo "Converting $1"
    num=${1:0:-2}
    if [[ "$1" =~ .*"um".* ]]; then
        echo "$num"
        # return $((${1:0:-2}))
    else if [[ "$1" =~ .*"mm".* ]]; then
        echo "$(expr $num*1000 | bc)"
        # return $((${1:0:-2}))
        fi
    fi
}

extract_alignment() {
    echo -e "\e[34;1mStarting alignment performance analysis.\e[0m"

    # Make a new csv file
    rm -f ${res_dir}/alignment_unsorted.csv
    touch ${res_dir}/alignment_unsorted.csv
    # Write headers into csv files
    echo "#run, thr, X, Y, Z, degX, degY, degZ" > ${res_dir}/alignment_unsorted.csv

    # Iterate over thresholds
    for run in $(seq ${run_n_start} ${run_n_end}); do
        # Skip run if either bad or is already included by an earlier split run
        if [[ "$(retrieve_status $run)" =~ "bad" ]] || [[ "$(retrieve_status $run)" =~ "warn" ]]; then
            continue
        fi

        # Skip if output file doesn't exist
        if [ ! -f ${geom_dir}/run${run}.conf ]; then
            continue
        fi

        echo -e "            \e[34;1mRun $run\e[0m (DAC $(retrieve_DAC ${run}), status $(retrieve_status ${run}))"

        # Line number for the proper detector config
        ln=$(cat ${geom_dir}/run${run}.conf | grep -n "\[dut\]" | awk '{split($0,a,":");print a[1]}')

        # Get true position
        pos=($(cat ${geom_dir}/run${run}.conf | head -n $((ln+15)) | tail -n 15 | awk '{ if($1 == "position") print $3,$4,$5}' | sed 's/,/ /g'))
        posX=$(convert_position ${pos[0]})
        posY=$(convert_position ${pos[1]})
        posZ=$(convert_position ${pos[2]})
        ang=($(cat ${geom_dir}/run${run}.conf | head -n $((ln+10)) | tail -n 10 | awk '{ if($1 == "orientation") print $3,$4,$5}' | sed 's/,/ /g' | sed 's/deg//g'))
        angX=${ang[0]}
        angY=${ang[1]}
        angZ=${ang[2]}

        echo "$run,$(retrieve_DAC ${run}),$posX,$posY,$posZ,$angX,$angY,$angZ" >> ${res_dir}/alignment_unsorted.csv
    done
        # Sort the efficiency file by threshold
        sort -t',' -k2,2 -g ${res_dir}/alignment_unsorted.csv -o ${res_dir}/alignment.csv

        rm ${res_dir}/alignment_unsorted.csv

}

extract_resolution () {
    echo -e "\e[34;1mExtracting spatial resolutions from analysis output files:\e[0m"

    # Make new csv file
    rm -f ${res_dir}/resolution_unsorted.csv
    touch ${res_dir}/resolution_unsorted.csv

    # Write csv file header
    echo -e "#run,thr,resolution,res_error" >> ${res_dir}/resolution_unsorted.csv

    # Variable to hold a threshold if it is to be skipped
    skip_DAC=""

    # Iterate over thresholds
    for run in $(seq ${run_n_start} ${run_n_end}); do
        # Skip run if either bad or is already included by an earlier split run
        if [[ "$(retrieve_status $run)" =~ "bad" ]] || [[ "$(retrieve_DAC $run)" == $skip_DAC ]]; then
            continue
        fi

        # Skip if output file doesn't exist
        if [ ! -f ${out_dir}/run${run}_analysis.root ]; then
            continue
        fi

        echo -en "            \e[34;1mRun $run\e[0m (DAC $(retrieve_DAC ${run}), status $(retrieve_status ${run})):"

        # Count the number of runs with this threshold (for split runs)
        thr=$(retrieve_DAC ${run})
        same_thr=( $(grep "[[:space:]]$thr[[:space:]]" ${data_dir}/map | grep $geom | grep -oE "^[0-9]*[[:space:]]") )
        same_thr_ok=("${same_thr[@]}")

        # Remove bad runs from the split run list
        for t in ${same_thr[@]}; do
            if [[ "$(retrieve_status $t)" =~ "bad" ]]; then
                same_thr_ok=( ${same_thr_ok[@]/$t} )
            fi
        done

        # If threshold is split across multiple runs, calculate combined cluster size means
        if [ ${#same_thr_ok[@]} -gt 1 ]; then
            skip_DAC=$(retrieve_DAC $run)
            echo -en "\n              - This run has a threshold that is split across ${#same_thr_ok[@]} good runs:\e[34;1m "
            for r in ${same_thr_ok[@]}; do echo -n "$r "; done
            echo -en "\e[0m\n              - Calculating combined resolution:"

            # Remove bad runs from the split run list
            runs=""
            for t in ${same_thr_ok[@]}; do
                runs+="$t,"
            done
            runs=${runs::-1}

            output=$(root -l -b -q "calc_resolution.C(\"${CAMPAIGN}\",\"${geom}\",\"$runs\")")
        else
            output=$(root -l -b -q "calc_resolution.C(\"${CAMPAIGN}\",\"${geom}\",\"$run\")")
        fi

        # echo $output
        res=$(echo $output | grep -oE "Resolution=.*," | grep -oE "[0-9.]*")
        error=$(echo $output | grep -oE "Error=.*\(" | grep -oE "[0-9.]*")
        echo " $res +- $error"

        # Write to csv file
        if [[ "$res" != "0" ]] && [[ ! -z "$res" ]]; then
            echo "$run,$(retrieve_DAC ${run}),$res,$error" >> ${res_dir}/resolution_unsorted.csv
        fi
    done

    # Sort the cluster size file by threshold
    sort -t',' -k2,2 -g ${res_dir}/resolution_unsorted.csv -o ${res_dir}/resolution.csv

    rm ${res_dir}/resolution_unsorted.csv
}


# ------------------------------
# -------- Main program --------
# ------------------------------


WORKERS=10
AVAIL_MEM_LIM=10
MAIN_PID=$$

# Corry executable
corry_exec=${CORRY_DIR}/corry

# Check corry executable is sourced
if [ -z  "${CORRY_DIR}" ]; then
    echo -e "\e[31mEnvironmental variable CORRY_DIR empty, please source the setup.sh script.\e[0m"
    exit 1
fi

# Help statement
if [[ "$1" == "-h" || "$1" == "--help" || -z "$1" || -z "$2" || -z "$3" ]]; then
    print_help
    exit 0
fi

# Config and data dirs
conf_dir=${PROJECT_DIR}/configs/${CAMPAIGN}
data_dir=${PROJECT_DIR}/data/${CAMPAIGN}

# Run numbers are passed by user
run_n_start=$1
run_n_end=$2
run_n_align=$3

# Check user input validity
geom_begin=$(retrieve_geometry ${run_n_start})
geom_end=$(retrieve_geometry ${run_n_end})
if [ ! "${geom_begin}" == "${geom_end}" ]; then
    echo -e "\e[31;1mRun numbers cover more than one geometry.\e[0m"
    exit 1
fi
if [[ ${run_n_start} -gt ${run_n_align} || ${run_n_end} -lt ${run_n_align} ]]; then
    echo -e "\e[31;1mInvalid alignment run number.\e[0m"
fi

# Other directories
geom=${geom_begin}
geom_dir=${PROJECT_DIR}/reco/${CAMPAIGN}/$geom/geometry
log_dir=${PROJECT_DIR}/reco/${CAMPAIGN}/$geom/logs
out_dir=${PROJECT_DIR}/reco/${CAMPAIGN}/$geom/output
res_dir=${PROJECT_DIR}/reco/${CAMPAIGN}/$geom/results

# Make directories
mkdir -p ${geom_dir} ${log_dir} ${out_dir} ${res_dir}

override=0

# Debug printout
echo " --------------- DEBUG INFO ---------------"
echo " Corry exec     : ${CORRY_DIR}/corry"
echo " Project dir    : ${PROJECT_DIR}"
echo " Campaign       : ${CAMPAIGN}"
echo " Config dir     : ${conf_dir}"
echo " Data dir       : ${data_dir}"
echo " Geometry       : $geom"
echo " Result dir     : ${res_dir}"
echo " Geometries dir : ${geom_dir}"
echo " Log dir        : ${log_dir}"
echo " Output dir     : ${out_dir}"
echo " Run start      : $1"
echo " Run end        : $2"
echo " Run align      : $3"
if [ $override -eq 1 ]; then
    echo -e "\e[33;1m Override ON !\e[0m"
fi
echo -e " ------------------------------------------"

# Run Corryvreckan reconstruction
run_masking
run_prealign
run_align 1 1
run_analysis

extract_efficiencies
extract_cluster_sizes
extract_alignment
extract_resolution
