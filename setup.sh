# Export project directory
export PROJECT_DIR=`pwd`

# APSQ implementation
export CAMPAIGN="June2022-unirradR2"
# export CAMPAIGN="August2022-SS"
# export CAMPAIGN="August2022-R2"
# export CAMPAIGN="Nov2023-R3"
# export CAMPAIGN="March2024-SS"
export CORRY_DIR=~/Git/ITk/corryvreckan/bin

echo -e "Exporting campaign name: \e[32m${CAMPAIGN}\e[0m"
echo -e "Exporting corry path:    \e[32m${CORRY_DIR}\e[0m"

# Check if corry bin exists in the path
if [ -f ${CORRY_DIR}/corry ]; then
    echo -e "\e[32mCorry executable exists in the path.\e[0m"
else
    echo -e "\e[31mCorry executable doesn't exist in the path, please check the path again.\e[0m"
    return 1
fi
