#!/bin/bash

# Directory with raw data files
data_dir=/eos/atlas/atlascerngroupdisk/det-itk/general/strips/Testbeam/August2022/raw_data/SS/
data_dir=~/Git/ITk/data/raw

# EUDAQ directory
eudaq_dir=$(realpath ../eudaq)

# Output directory for synced data
sync_dir=$(realpath synced_runs)
sync_dir=~/Git/ITk/data/synced
mkdir -p ${sync_dir}

# Debug printout
echo -e " -------- Debug info --------"
echo -e "  Data dir     : ${data_dir}"
echo -e "  EUDAQ dir    : ${eudaq_dir}"
echo -e "  Sync dir     : ${sync_dir}"
echo -e "  Run range    : $1-$2"
echo -e " ----------------------------\e[0m"

echo -e "\e[34;1mStarting resynchronization of runs.\e[0m"
for ((i=$1; i<=$2; i++)); do
	echo -e "\e[34;1mSubmitting resync of run $i.\e[0m"

	# Check if output file already exists
	if [ -f ${sync_dir}/run00$i.raw ]; then
		echo -e "\e[33m   Resynced data file for run $i already exists, overwriting.\e[0m"
		# Remove output file so that resynchronizer doesn't complain
		rm ${sync_dir}/run00$i.raw
	fi

	# Check if input raw file exists
	if [ $(ls ${data_dir} | grep run00$i | wc -l) -eq 0 ]; then
		echo -e "\e[31;1m  Raw file for run $i doesn't exist.\e[0m"
		continue
	else
		# Main EUDAQ resync command
        ${eudaq_dir}/bin/euCliReSynchroniser -d -i\
        ${data_dir}/run00$i*.raw -o\
        ${sync_dir}/run00$i.raw \
        > ${sync_dir}/run00$i.log
	fi

	# Read resync result and parse events numbers and efficiencies
	if [ -f ${sync_dir}/run00$i.log ]; then
		result=$(cat ${sync_dir}/run00$i.log | grep -oE "Finished processing and writing [0-9]* events")
		echo "   $result"
	else
		echo -e "\e[31;1m  Log file for run $i doesn't exist.\e[0m"
	fi

	# Check if resynced data file was produced
	if [ -f ${sync_dir}/run00$i.raw ]; then
		echo -e "   Resynced data file produced."
	else
		echo -e "\e[31;1m   Resynced data file not produced, try different eudaq branch or resync parameter.\e[0m"
	fi
done
