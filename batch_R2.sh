# Source setup if was not done before
if [ -z "$CAMPAIGN" ];then
    source setup.sh
fi

# -------------------
# --- CALIBRATION ---
# -------------------
root -l -b -q "charge_calibration.C(\"$CAMPAIGN\")"


# -----------------------
# --- NOISE OCCUPANCY ---
# -----------------------
root -l -b -q "noise_occupancy.C(\"$CAMPAIGN\")"


# -----------------------
# --- RECONSTRUCTIONS ---
# -----------------------
bash run.sh 2081 2120 2081  # 5deg
bash run.sh 2121 2151 2139  # 10deg
bash run.sh 2154 2181 2168  # 15deg
bash run.sh 2183 2216 2198  # 20deg
bash run.sh 2217 2251 2235  # 25deg
bash run.sh 968 990 977 # s30
bash run.sh 1016 1056 1030 # s31
bash run.sh 936 967 943 # s32
bash run.sh 2293 2326 2310 # s33

# Rerun low thresholds with fixed high-thr geom
# bash run.sh 2083 2090 2081  # 5deg
# bash run.sh 2123 2133 2139  # 10deg
# bash run.sh 2154 2161 2168  # 15deg
# bash run.sh 2185 2191 2198  # 20deg
# bash run.sh 2218 2229 2235  # 25deg
# bash run.sh 969 973 977 # s30
# bash run.sh 1017 1025 1030 # s31
# bash run.sh 934 937 943 # s32
# bash run.sh 2294 2303 2310 # s33


# ---------------------
# --- RUN SUMMARIES ---
# ---------------------
GEOM="R2_05deg"
for runN in $(seq 2081 2120); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_10deg"
for runN in $(seq 2121 2151); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_15deg"
for runN in $(seq 2154 2181); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_20deg"
for runN in $(seq 2183 2216); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_25deg"
for runN in $(seq 2217 2251); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_s32"
for runN in $(seq 936 967); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_s30"
for runN in $(seq 968 990); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_s31"
for runN in $(seq 1015 1056); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*

GEOM="R2_s33"
for runN in $(seq 2293 2326); do
    run_info=(`grep $runN data/$CAMPAIGN/map`)
    root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
done
pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
rm reco/$CAMPAIGN/$GEOM/results/temp*


# -----------------------
# --- PROCESS RESULTS ---
# -----------------------
TITLE="June2022-unirradR2 - 5deg, s33, ASICs 0,1"
GEOM="R2_05deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"do\",\"0-1\",\"$TITLE\")"

TITLE="June2022-unirradR2 - 10deg, s33, ASICs 0,1"
GEOM="R2_10deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"do\",\"0-1\",\"$TITLE\")"

TITLE="June2022-unirradR2 - 15deg, s33, ASICs 0,1"
GEOM="R2_15deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"do\",\"0-1\",\"$TITLE\")"

TITLE="June2022-unirradR2 - 20deg, s33, ASICs 0,1"
GEOM="R2_20deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"do\",\"0-1\",\"$TITLE\")"

TITLE="June2022-unirradR2 - 25deg, s33, ASICs 0,1"
GEOM="R2_25deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"do\",\"0-1\",\"$TITLE\")"

TITLE="June2022-unirradR2 (perpendicular), s30, ASICs 2,3"
GEOM="R2_s30"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",0,\"up\",\"2-3\",\"$TITLE\")"

TITLE="June2022-unirradR2 (perpendicular), s31, ASICs 2,3"
GEOM="R2_s31"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",0,\"do\",\"2-3\",\"$TITLE\")"

TITLE="June2022-unirradR2 (perpendicular), s32, ASICs 2,3"
GEOM="R2_s32"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"up\",\"2-3\",\"$TITLE\")"

TITLE="June2022-unirradR2 (perpendicular), s33, ASICs 2,3"
GEOM="R2_s33"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"do\",\"2-3\",\"$TITLE\")"
