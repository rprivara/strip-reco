# Source setup if was not done before
if [ -z "$CAMPAIGN" ];then
    source setup.sh
fi

# -------------------
# --- CALIBRATION ---
# -------------------
root -l -b -q "charge_calibration.C(\"$CAMPAIGN\")"


# -----------------------
# --- NOISE OCCUPANCY ---
# -----------------------
root -l -b -q "noise_occupancy.C(\"$CAMPAIGN\")"


# -----------------------
# --- RECONSTRUCTIONS ---
# -----------------------
# bash run.sh 2851 2888 2870  #s30_right
# bash run.sh 2971 3005 2990  #s31_left
# bash run.sh 2891 2927 2911  #s32_right
# bash run.sh 2929 2968 2953  #s33


# ---------------------
# --- RUN SUMMARIES ---
# ---------------------
# GEOM="s30_right"
# for runN in $(seq 2851 2888); do
#     run_info=(`grep $runN data/$CAMPAIGN/map`)
#     root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
# done
# pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
# rm reco/$CAMPAIGN/$GEOM/results/temp*

# GEOM="s31_left"
# for runN in $(seq 2971 3005); do
#     run_info=(`grep $runN data/$CAMPAIGN/map`)
#     root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
# done
# pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
# rm reco/$CAMPAIGN/$GEOM/results/temp*

# GEOM="s32_right"
# for runN in $(seq 2891 2927); do
#     run_info=(`grep $runN data/$CAMPAIGN/map`)
#     root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
# done
# pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
# rm reco/$CAMPAIGN/$GEOM/results/temp*

# GEOM="s33"
# for runN in $(seq 2929 2968); do
#     run_info=(`grep $runN data/$CAMPAIGN/map`)
#     root -l -b -q "gen_summary.C(\"$CAMPAIGN\",\"$GEOM\",$runN,${run_info[2]},\"${run_info[3]}\")" > /dev/null
# done
# pdfunite reco/$CAMPAIGN/$GEOM/results/temp* reco/$CAMPAIGN/$GEOM/results/run_summary.pdf
# rm reco/$CAMPAIGN/$GEOM/results/temp*


# -----------------------
# --- PROCESS RESULTS ---
# -----------------------
TITLE="August2022-SS (perpendicular), s30, ASICs 2,3"
GEOM="s30_right"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",0,\"up\",\"2-3\",\"$TITLE\")"

TITLE="August2022-SS (perpendicular), s31, ASICs 7,8"
GEOM="s31_left"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",0,\"do\",\"7-8\",\"$TITLE\")"

TITLE="August2022-SS (perpendicular), s32, ASICs 6,7"
GEOM="s32_right"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"do\",\"6-7\",\"$TITLE\")"

TITLE="August2022-SS (perpendicular), s33, ASICs 1,2"
GEOM="s33"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",1,\"up\",\"1-2\",\"$TITLE\")"


# ----------------------
# --- GENERATE PLOTS ---
# ----------------------
# root -l -b -q "gen_plots.C(\"$CAMPAIGN\")"
