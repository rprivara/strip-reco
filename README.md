# strip-reco
Reconstruction and analysis of ITk Strip test beam data

# Subdirectories in this repository
## configs
Contains corryvreckan configuration files for each reconstruction step:
- `0-mask.conf` - Masking of noisy strips/pixels
- `1-prealign.conf` - Pre-aligns detectors based on correlation plots.
- `2-align1.conf` - Iteration 1 of telescope alignment.
- `3-align2.conf` - Iteration 2 of telescope alignment.
- `4-align3.conf` - Iteration 1 of DUT alignment.
- `5-align4.conf` - Iteration 2 of DUT alignment.
- `6-analyze.conf` - Runs analysis of the aligned run

Additionally, the `geometries` folder contains the descriptions of experimental geometry for each threshold scan done during test beam measurements. The name of the geometry files has to correspond to the name listed in the data map, e.g. if a data map for `March2023-LS-TrueBlue` measurement lists geometry for a given run tagged with the geometry `s21_left`, the script will look for `s21_left.conf` geometry file in the `geometries` directory.

## data
Contains directories for individual measurements during a test beam campaign. Each directory should contain a `map` file, a text file where each line contains information about a specific run number (first column), geometry name, charge threshold and run status (`ok`, `warn`, or `bad`). Reconstruction scripts look up information about a given run from this map.

The `data` directory is also supposed to store the raw run data, however due to their size, this cannot be included in the repository itself and can be accessed on machines connected to EOS. The usual path to raw data is:
`/eos/atlas/atlascerngroupdisk/det-itk/general/strips/Testbeam`.
Data is sorted there by campaigns.

## reco
Contains output created by the reconstruction process. A new directory will be created for each campaign, defined in the main `run.sh` script. Subdirectories for each geometry will be also created inside. Each of these subdirectories will have the following structure:
- `geometry` - stores geometry files produced by the alignment process.
- `logs` - various logs outputted by the reconstruction steps
- `output` - ROOT files with detailed outputs, produced by the reconstruction steps
- `results` - final efficiency and noise occupancy plots, produced by `get_efficiency.C` and `get_NO.C` scripts.

# How to run
First, the path to the corryvreckan binary should be exported via the `CORRY_DIR` environmental variable. This is preferably done using the `setup.sh` script by uncommenting and editing line 16. The script additionally checks that the binary exists in the exported path and also exports the path to the current project directory as `PROJECT_DIR` variable.

The reconstruction is started by running the `run.sh` script with 3 additional parameters:
1. the number of the first run to be reconstructed,
2. the number of the last run to be reconstructed, and
3. the number of the run to be used for masking and telescope alignment.

These run numbers must be listed in the `map` file of the given campaign. For example, to run reconstruction of runs 6289 to 6340 taken during the `March2023-LS-TrueBlue` campaign, while using run number 6319 for telescope alignment, the command would be:
```$ bash run.sh 6289 6340 6319```

This should eventually produce a .csv file in the `reco/[campaign]/[geometry]/results` subdirectory, which lists the run number, charge threshold, reconstructed efficiency and lower and upper efficiency errors for all requested and reconstructed runs.

The ROOT file containing the measured noise occupancy should be placed into the `data/[campaign]` folder and named `rawNO.root`. The ROOT macro `get_NO.C` can then be run with one parameter, the name of the campaign:
```$ root -l -b -q 'get_NO.C("March2023-LS-TrueBlue")'```
This generates the file `data/[campaign]/NO.root` with the processed noise occupancy.

The efficiency can then be plotted using the `get_efficiency.C` ROOT macro with 3 parameters:
1. campaign name,
2. geometry, and
3. plot title.

For example, to plot the efficiency for `March2023-LS-TrueBlue` campaign and for runs tagged as `s21_left` geometry with the title "efficiency vs NO", the command would be
```root -l -b -q 'get_efficiency.C("March2023-LS-TrueBlue","s21_left","efficiency vs NO")'```
(based on the terminal or script from where this is run, the inner quotation marks might or might not have to be escaped using the backslash symbol `\`).

This will finally produce a ROOT file with the efficiency vs noise occupancy graph and .pdf plots in the `reco/[campaign]/[geometry]/results` directory.
