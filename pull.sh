#/bin/bash

if [[ "$1" == "-h" || -z "$1" || -z "$2" ]]; then
    echo "Usage: pull.sh [RUN_FIRST] [RUN_LAST]"
    exit 1
fi

# First and last run to copy
RUN_FIRST=$1
RUN_LAST=$2

# File name base
FILE_NAME="run00"

# Build pattern file
echo -e "\e[34;1mBuilding rsync pattern list for runs ${RUN_FIRST} to ${RUN_LAST}.\e[0m"
PATTERN_FILE="rsync_pattern.txt"
rm -f ${PATTERN_FILE}
touch ${PATTERN_FILE}
for RUN_N in $(seq ${RUN_FIRST} ${RUN_LAST}); do
    echo -e "${FILE_NAME}${RUN_N}*" >> ${PATTERN_FILE}
done

TB_DIR="/eos/atlas/atlascerngroupdisk/det-itk/general/strips/Testbeam/August2022/raw_data/SS/"
DATA_DIR="/home/$USER/Git/ITk/data/raw/"

# Run rsync
echo -e "\e[34;1mDownloading data files.\e[0m"
rsync -avzhe ssh --progress --include-from=${PATTERN_FILE} --exclude='*.*' rprivara@lxplus.cern.ch:${TB_DIR} ${DATA_DIR}

# Rename copied files
RENAME=0
TARGET_NAME="run00"
if [ $RENAME -eq 1 ]; then
    echo -e "\e[34;1mRenaming copied files.\e[0m"
    for FILE in $(ls ${DATA_DIR} | grep "^${FILE_NAME}[0-9]*_"); do
        TARGET_NAME="$(echo ${FILE::-17}.raw | grep -oE 'run[0-9]*.raw')"
        # echo -e " - $FILE  ->  ${TARGET_NAME}"
        mv ${DATA_DIR}/$FILE ${DATA_DIR}/${TARGET_NAME}
    done
fi

# Remove pattern file
rm -f ${PATTERN_FILE}
